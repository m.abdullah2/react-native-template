import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import { AppStyles, Images } from '@theme';
import { ButtonView } from '@components';
import { strings } from '@il8n';

import styles from './styles';

const ErrorViewApi = ({ errorMessage, onPressRetry, containerStyle }) => {
  return (
    <View
      style={[AppStyles.container, AppStyles.alignCenterView, containerStyle]}>
      <Image source={Images.networkError} style={styles.image} />
      <Text style={styles.errorMessage}>{errorMessage}</Text>
      <ButtonView onPress={onPressRetry} style={styles.retryButton}>
        <Text>{strings('api_error_messages.retry').toLocaleUpperCase()}</Text>
      </ButtonView>
    </View>
  );
};

ErrorViewApi.propTypes = {
  errorMessage: PropTypes.string.isRequired,
  onPressRetry: PropTypes.func.isRequired,
};

ErrorViewApi.defaultProps = { containerStyle: {} };

export default ErrorViewApi;
