import { StyleSheet } from 'react-native';

import { Fonts, Colors, Metrics } from '@theme';

export default StyleSheet.create({
  container: { marginBottom: Metrics.ratio(17) },
  title: {
    marginBottom: Metrics.ratio(12, 9),
    // fontFamily: Fonts.type.bold,
    fontWeight: 'bold',
    fontSize: Fonts.size.size_14,
    color: Colors.primary,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  arrowStyle: { marginRight: Metrics.baseMargin },
  input: {
    paddingBottom: Metrics.ratio(15, 12),
    paddingTop: Metrics.ratio(15, 12),
    paddingHorizontal: Metrics.ratio(15),
    flex: 1,
    fontSize: Fonts.size.size_15,
    color: Colors.black,
    // fontFamily: Fonts.type.regular,
    fontWeight: '400',
    includeFontPadding: false,
  },
  leftImage: {
    width: Metrics.ratio(25),
    height: Metrics.ratio(25),
    marginLeft: Metrics.ratio(15),

    resizeMode: 'contain',
  },
  placeHolderImage: {
    width: Metrics.ratio(25),
    height: Metrics.ratio(25),
    // marginLeft: Metrics.ratio(15),

    resizeMode: 'contain',
  },
  multline: {
    height: Metrics.ratio(90),
    //marginBottom: Metrics.ratio(-12, 2),
  },
  bottomContainer: { flexDirection: 'row', marginTop: Metrics.ratio(10) },
  errorView: { flex: 1, marginRight: Metrics.smallMargin },
  eyeContainer: { padding: Metrics.midMargin },
  titleView: { flexDirection: 'row' },
  optionalText: { marginLeft: Metrics.ratio(8), color: Colors.pineappleGrey },
});
