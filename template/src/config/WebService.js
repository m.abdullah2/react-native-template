// BASE URL
export const BASE_URL = 'https://jsonplaceholder.typicode.com';
export const X_API_TOKEN = 'X-Access-Token';

// REQUEST TYPES
export const REQUEST_TYPE = {
  GET: 'get',
  POST: 'post',
  DELETE: 'delete',
  PUT: 'put',
  PATCH: 'patch',
};

// CONSTANTS
export const LIMIT = 20;
export const API_TIMEOUT = 30000;
export const API = '/api/';
export const API_LOG = true;

// API'S

export const POST_LISTING = {
  route: 'https://jsonplaceholder.typicode.com/posts',
  access_token_required: false,
  type: REQUEST_TYPE.POST,
};

export const GET_POST = {
  route: 'https://jsonplaceholder.typicode.com/posts',
  access_token_required: false,
  type: REQUEST_TYPE.GET,
};
