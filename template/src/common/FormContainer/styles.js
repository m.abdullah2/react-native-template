import { StyleSheet } from 'react-native';

import { Colors, Fonts, Metrics } from '@theme';

export default StyleSheet.create({
  container: {
    paddingBottom: Metrics.bottomPadding,
  },
  button: {
    // marginTop: Metrics.ratio(20),
    // shadowColor: Colors.black,
    // shadowOpacity: 0.2,
    // shadowOffset: {
    //   width: 0,
    //   height: 3,
    // },
    // shadowRadius: 3,
    elevation: 3,
    borderRadius: Metrics.ratio(5),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: Metrics.ratio(55),
    backgroundColor: Colors.primary,
    marginHorizontal: Metrics.ratio(20),
    marginBottom: Metrics.bottomPadding,
  },
  // button: {
  //   borderRadius: Metrics.ratio(5),
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   flexDirection: 'row',
  //   height: Metrics.ratio(45),
  // },
  title: {
    // fontFamily: Fonts.type.medium,
    fontWeight: '400',
    fontSize: Fonts.size.size_17,
    textAlign: 'center',
    color: Colors.white,
  },
});
