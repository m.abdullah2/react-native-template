import * as RNLocalize from 'react-native-localize';
import { I18nManager } from 'react-native';
import i18n from 'i18n-js';

import en from '../translations/en.json';
import ar from '../translations/ar.json';

// set if you to set language from OS level settings
i18n.locale = RNLocalize?.getLocales()?.[0]?.languageCode ?? 'en';

// default to en
i18n.fallbacks = true;
i18n.translations = { en, ar };

// The method we'll use instead of a regular string
export function strings(name, replace = undefined, params = {}) {
  let stringValue = i18n.t(name, params);
  // replace strings
  if (replace) {
    for (const property in replace) {
      stringValue = stringValue.replace(`{${property}}`, replace[property]);
    }
  }
  return stringValue;
}

// set language in il8n
export function setLocal(locale = 'en') {
  i18n.locale = locale;
}

// get current language in il8n
export function getLocale() {
  return i18n.locale;
}

// force rtl
export function forceRTL(isRTL) {
  I18nManager.forceRTL(isRTL);
}

export default { strings, setLocal, forceRTL, getLocale };
