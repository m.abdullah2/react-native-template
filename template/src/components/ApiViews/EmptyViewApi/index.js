import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import { AppStyles } from '@theme';
import { strings } from '@il8n';

import styles from './styles';

const EmptyViewApi = ({ text, description, image }) => {
  return (
    <View style={[AppStyles.container, AppStyles.alignCenterView]}>
      {image ? <Image source={image} style={styles.image} /> : null}
      <Text style={styles.text}>
        {text || strings('api_error_messages.no_record_found')}
      </Text>
      {description ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
    </View>
  );
};

EmptyViewApi.propTypes = {
  text: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.number,
};

EmptyViewApi.defaultProps = {
  text: '',
  description: '',
  image: undefined,
};

export default EmptyViewApi;
