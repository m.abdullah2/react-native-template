import { ActivityIndicator, View } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import { Colors, AppStyles } from '@theme';

const LoaderViewApi = ({ style, size, animating }) => (
  <View style={[AppStyles.container, AppStyles.alignCenterView, style]}>
    <ActivityIndicator {...{ size, animating }} color={Colors.primary} />
  </View>
);

LoaderViewApi.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  size: PropTypes.oneOf(['small', 'large']),
  animating: PropTypes.bool,
};

LoaderViewApi.defaultProps = { size: 'large', animating: true };

export default LoaderViewApi;
