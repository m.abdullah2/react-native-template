import { combineReducers } from 'redux';

import requestFlags from './requestFlags';
import network from './network';

import addPost from './addPost';

const appReducer = combineReducers({
  requestFlags,
  network,
  addPost,
});

export default appReducer;
