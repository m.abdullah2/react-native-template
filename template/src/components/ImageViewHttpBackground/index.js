import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import ImageViewHttp from '../ImageViewHttp';
import { Metrics, Colors } from '@theme';

const ImageViewHttpBackground = ({
  width,
  height,
  borderRadius,
  containerStyle,
  url,
  children,
  contentStyle,
  isLocal,
}) => {
  return (
    <View
      style={[
        { width, height, borderRadius, overflow: 'hidden' },
        containerStyle,
      ]}>
      <ImageViewHttp
        style={{ width, height }}
        placeholderStyle={{ width: width / 2, height: height / 2 }}
        {...{ url, isLocal }}
      />
      <View style={[styles.contentStyle, contentStyle]}>{children}</View>
    </View>
  );
};

ImageViewHttpBackground.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  url: PropTypes.string,
  borderRadius: PropTypes.any,
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  contentStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  isLocal: PropTypes.bool,
};

ImageViewHttpBackground.defaultProps = {
  style: {},
  borderRadius: Metrics.ratio(12),
  url: '',
  containerStyle: {},
  contentStyle: {},
  isLocal: false,
};

export default React.memo(ImageViewHttpBackground);

const styles = StyleSheet.create({
  contentStyle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: Colors.black02,
    padding: Metrics.ratio(10),
  },
});
