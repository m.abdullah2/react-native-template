import { StyleSheet } from 'react-native';
import { Metrics, Colors, Fonts } from '@theme';

export default StyleSheet.create({
  errorMessage: {
    width: '80%',
    textAlign: 'center',
    lineHeight: Metrics.ratio(24),
    marginTop: Metrics.ratio(32),
    marginBottom: Metrics.ratio(24),
    fontSize: Fonts.size.size_16,
    // fontFamily: Fonts.type.medium,
    fontWeight: '400',
  },
  retryButton: {
    paddingVertical: Metrics.midMargin,
    paddingHorizontal: Metrics.baseMargin * 2,
    borderRadius: Metrics.ratio(4),
    borderColor: Colors.black,
    borderWidth: Metrics.ratio(1),
    marginTop: Metrics.baseMargin,
  },
  image: {},
});
