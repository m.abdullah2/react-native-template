module.exports = {
  root: true,
  extends: [
    '@react-native-community',
    'plugin:import/recommended',
    'plugin:react-hooks/recommended',
  ],
  settings: {
    'import/ignore': ['node_modules'],
    'import/resolver': {
      alias: {
        map: [
          ['@assets', './src/assets'],
          ['@chatUtils', './src/chatUtils'],
          ['@common', './src/common'],
          ['@components', './src/components'],
          ['@config', './src/config'],
          ['@jsonData', './src/jsonData'],
          ['@dataUtils', './src/dataUtils'],
          ['@ducks', './src/ducks'],
          ['@models', './src/models'],
          ['@navigator', './src/navigator'],
          ['@screens', './src/screens'],
          ['@store', './src/store'],
          ['@theme', './src/theme'],
          ['@translations', './src/translations'],
          ['@utils', './src/utils'],
          ['@il8n', './src/utils/i18n'],
          ['@ActionTypes', './src/ducks/ActionTypes'],
          ['@WebService', './src/config/WebService'],
          ['@Constants', './src/config/Constants'],
          ['@ApiSauce', './src/utils/ApiSauce'],
          ['@ValidationUtil', './src/utils/ValidationUtil'],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json'],
      },
    },
  },
};
