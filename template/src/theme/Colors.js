export default {
  primary: '#191970',
  secondary: '#ff8200',
  placeholderImageColor: '#eaeef1',
  black: '#000000',
  white: '#ffffff',
  black02: 'rgba(0,0,0,0.2)',
  black005: 'rgba(0,0,0,0.05)',
};
