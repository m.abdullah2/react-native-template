import { take, put, fork, call } from 'redux-saga/effects';

import { POST_LISTING, GET_POST } from '@WebService';
import { callRequest } from '@ApiSauce';

import { addPost, getPostListing } from './';
import { NavigationService, Util } from '@utils';

function* watchAddPost() {
  while (true) {
    const { payload } = yield take(addPost.request.type);
    const { payloadApi } = payload;
    try {
      const response = yield call(callRequest, POST_LISTING, payloadApi);
      yield put(
        addPost.success({
          data: response,
        }),
      );
      NavigationService.reset('Home');
      Util.showMessage('Post added successfully', 'success');
    } catch (error) {
      yield put(addPost.failure({ errorMessage: error.message }));
      Util.showMessage(error.message);
    }
  }
}

function* watchGetPost() {
  while (true) {
    yield take(getPostListing.request.type);

    try {
      const response = yield call(callRequest, GET_POST);
      yield put(
        getPostListing.success({
          data: response,
        }),
      );
    } catch (error) {
      yield put(getPostListing.failure({ errorMessage: error.message }));
    }
  }
}

export default function* root() {
  yield fork(watchAddPost);
  yield fork(watchGetPost);
}
