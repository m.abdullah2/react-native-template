import { StyleSheet } from 'react-native';
import { Colors, Fonts, Metrics } from '@theme';

export default StyleSheet.create({
  headerStyle: {
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: Colors.lightWhite,
  },
  headerHeight: {
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: Colors.lightWhite,
    height: 60,
  },
  headerStyleWhite: {
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: Colors.white,
  },
  headerTitleStyle: {
    // fontFamily: Fonts.type.medium,
    fontWeight: '400',
    fontSize: Fonts.size.size_17,
    marginHorizontal: Metrics.baseMargin,
  },
});
