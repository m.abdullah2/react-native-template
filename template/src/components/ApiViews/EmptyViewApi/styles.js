import { StyleSheet } from 'react-native';
import { Metrics, Fonts, Colors } from '@theme';

export default StyleSheet.create({
  text: {
    // fontFamily: Fonts.type.medium,
    fontWeight: '400',
    fontSize: Fonts.size.size_18,
    color: Colors.black,
  },
  image: { marginBottom: Metrics.ratio(38) },
  description: {
    // fontFamily: Fonts.type.regular,
    fontWeight: '400',
    fontSize: Fonts.size.size_16,
    color: Colors.black,
    marginTop: Metrics.baseMargin,
  },
});
