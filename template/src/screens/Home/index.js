import { getPostList, getPostListing } from '@ducks/addPost';
import { Text, View } from 'react-native';
import { AppStyles } from '@theme';

import { FlatListApi } from '@components';
import styles from './styles';
import React from 'react';

const Home = () => {
  //render title
  const renderTitle = () => {
    return (
      <View style={AppStyles.titleView}>
        <Text style={AppStyles.titleText}>Home</Text>
      </View>
    );
  };

  //render item
  const renderitem = item => {
    return (
      <View style={styles.rendeItem}>
        <Text style={styles.renderItemTitle}>{item?.title ?? ''}</Text>
        <Text>{item?.body ?? ''}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {renderTitle()}
      <FlatListApi
        requestAction={getPostListing.request}
        actionType={getPostListing.type}
        renderItem={({ item }) => renderitem(item)}
        selectorData={getPostList}
        keyExtractor={(item, index) => `${item?._id ?? index}`}
      />
    </View>
  );
};

export default Home;
