import { StyleSheet, Platform } from 'react-native';

import Metrics from './Metrics';
import Colors from './Colors';
import Fonts from './Fonts';

export default StyleSheet.create({
  flex1: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container2: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: Metrics.ratio(20),
  },
  container3: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: Metrics.ratio(20),
  },
  container4: {
    backgroundColor: Colors.white,
    paddingHorizontal: Metrics.ratio(20),
  },
  contentContainerBottom: {
    paddingBottom: Metrics.bottomPadding,
  },
  titleView: {
    backgroundColor: Colors.primary,
    height: Metrics.screenHeight * 0.08,
    justifyContent: 'center',
  },
  titleText: {
    fontSize: Fonts.size.size_20,
    color: 'white',
    textAlign: 'center',
    marginTop: Metrics.ratio(25),
    fontWeight: 'bold',
  },
  unreadCircle: {
    width: Metrics.ratio(8),
    height: Metrics.ratio(8),
    borderRadius: Metrics.ratio(4),
    backgroundColor: Colors.primary,
  },
  contentContainer: {
    paddingTop: Metrics.ratio(30),
    paddingBottom: Metrics.bottomPadding,
  },
  contentContainer2: {
    paddingTop: Metrics.ratio(30),
    paddingBottom: Metrics.bottomPadding,
    paddingHorizontal: Metrics.ratio(20),
  },
  flashMessage: {
    // fontFamily: Fonts.type.semiBold,
    fontWeight: 'bold',
    fontSize: Fonts.size.size_16,
    lineHeight: 22,
    paddingTop: Platform.select({ ios: 0, android: Metrics.statusBarHeight }),
  },
  empty: {},
  alignCenterView: { justifyContent: 'center', alignItems: 'center' },
  flatlistContentContainer: {
    // paddingHorizontal: Metrics.ratio(20),
    // paddingVertical: Metrics.ratio(28),
  },
  inputBoxStyle: {
    borderRadius: Metrics.ratio(5),
    borderColor: Colors.paleGrey,
    backgroundColor: '#f8f4f4',
  },
  imageBorderStyle: {
    borderColor: Colors.primary,
    borderWidth: Metrics.ratio(2),
  },
  padding: {
    padding: Metrics.ratio(20),
  },
  paddingHorizontal: {
    paddingHorizontal: Metrics.ratio(20),
  },
  paddingVertical: {
    paddingVertical: Metrics.ratio(20),
  },
  paddingTop: {
    paddingTop: Metrics.ratio(20),
  },
  margin: {
    margin: Metrics.ratio(20),
  },
  marginVertical: {
    marginVertical: Metrics.ratio(20),
  },
  marginHorizontal: {
    marginHorizontal: Metrics.ratio(20),
  },
  marginTop: {
    marginTop: Metrics.ratio(20),
  },
  profileContainer: {
    marginHorizontal: Metrics.ratio(20),
    marginVertical: Metrics.ratio(10),
  },
});
