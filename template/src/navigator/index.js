import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';

import { Home, Welcome } from '@screens';
import { NavigationService } from '@utils';
import { hideHeaderOptions, screenOptions } from './config';

// init stack
const Stack = createStackNavigator();

// main stack
function MyStack() {
  return (
    <Stack.Navigator screenOptions={screenOptions} initialRouteName={'Welcome'}>
      <Stack.Screen
        name="Welcome"
        component={Welcome}
        options={hideHeaderOptions}
      />
      <Stack.Screen name="Home" component={Home} options={hideHeaderOptions} />
    </Stack.Navigator>
  );
}

// main App Container
const AppContainer = () => {
  return (
    <NavigationContainer ref={NavigationService.navigationRef}>
      <MyStack />
    </NavigationContainer>
  );
};

export default AppContainer;
