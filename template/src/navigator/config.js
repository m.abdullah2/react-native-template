import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import {
  TransitionPresets,
  CardStyleInterpolators,
} from '@react-navigation/stack';

import styles from './styles';

export const sharedElementOptions = () => ({
  presentation: 'modal',
  headerShown: false,
  ...TransitionPresets.ScaleFromCenterAndroid,
});

export const screenOptions = () => ({
  // headerMode: 'screen',
  headerBackTitleVisible: false,
  headerStyle: styles.headerStyle,
  headerTitleAlign: 'center',
  headerTitleStyle: styles.headerTitleStyle,
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
  ...TransitionPresets.SlideFromRightIOS,
});

export const searchScreenOptions = () => ({
  headerBackTitleVisible: false,
  headerStyle: styles.headerStyle,
  headerTitleAlign: 'center',
  headerTitleStyle: styles.headerTitleStyle,
  animation: false,
});

export const modalScreenOptionsIOS = () => ({
  headerBackTitleVisible: false,
  headerStyle: styles.headerHeight,
  headerStatusBarHeight: 0,
  headerTitleAlign: 'center',
  headerTitleStyle: styles.headerTitleStyle,
  ...TransitionPresets.SlideFromRightIOS,
});

export const tabBarDetailOptions = ({ route }) => {
  const routeName = getFocusedRouteNameFromRoute(route) ?? '';
  if (routeName === 'LeadsDetails') {
    return { tabBarStyle: { display: 'none' } };
  }
  return {};
};

export const modalOptions = () => ({
  headerShown: false,
  presentation: 'modal',
  ...transitionModal,
});

export const screenOptionsWhite = () => ({
  headerBackTitleVisible: false,
  headerTitleAlign: 'center',
  headerStyle: styles.headerStyleWhite,
  headerTitleStyle: styles.headerTitleStyle,
  ...TransitionPresets.SlideFromRightIOS,
});

export const hideHeaderOptions = {
  headerShown: false,
};
