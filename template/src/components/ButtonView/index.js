import { TouchableOpacity, Platform } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

var disableClick = false;

const ButtonView = props => {
  const {
    style,
    children,
    onPress,
    isBackgroundBorderLess,
    disableRipple,
    debounceTime,
    rippleColor,
    disabled,
    disabledOpacity,
    disableDebounce,
    isHeightLight,
    activeOpacity,
    enableClick,
    ...rest
  } = props;

  const _onPress = () => {
    if (enableClick && onPress) {
      onPress();
    } else if (disableClick) {
    } else {
      disableClick = true;
      onPress?.();
      setTimeout(() => {
        disableClick = false;
      }, debounceTime);
    }
  };

  const opacityStyle = disabled ? { opacity: disabledOpacity } : {};

  return (
    <TouchableOpacity
      style={[style, opacityStyle]}
      disabled={disabled}
      {...rest}
      onPress={_onPress}
      activeOpacity={activeOpacity}>
      {children}
    </TouchableOpacity>
  );
};

ButtonView.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  children: PropTypes.node.isRequired,
  onPress: PropTypes.func.isRequired,
  isBackgroundBorderLess: PropTypes.bool,
  disableRipple: PropTypes.bool,
  debounceTime: PropTypes.number,
  rippleColor: PropTypes.string,
  disabled: PropTypes.bool,
  disabledOpacity: PropTypes.number,
  activeOpacity: PropTypes.number,
  disableDebounce: PropTypes.bool,
  isHeightLight: PropTypes.bool,
};

ButtonView.defaultProps = {
  style: {},
  isBackgroundBorderLess: false,
  disableRipple: false,
  debounceTime: Platform.select({ android: 700, ios: 200 }),
  rippleColor: '',
  disabled: false,
  disabledOpacity: 0.5,
  activeOpacity: 0.5,
  disableDebounce: false,
  isHeightLight: false,
};

export default ButtonView;
