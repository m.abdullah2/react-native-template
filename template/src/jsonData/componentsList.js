import { Images } from '@theme';

export default [
  {
    id: '1',
    title: 'Text 1 Sample',
    body: 'TextInputsSample',
  },
  {
    id: '2',
    title: 'Text 2 Sample',
    body: 'TextInputsSample',
  },
  {
    id: '3',
    title: 'Text 3 Sample',
    body: 'TextInputsSample',
  },
];
