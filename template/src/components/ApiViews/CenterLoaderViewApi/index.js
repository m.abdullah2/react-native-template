import { ActivityIndicator, View } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import { Colors, AppStyles } from '@theme';

const CenterLoaderViewApi = ({ style, size, animating }) => {
  return (
    <View style={[AppStyles.container, AppStyles.alignCenterView, style]}>
      <ActivityIndicator {...{ size, animating }} color={Colors.primary} />
    </View>
  );
};

CenterLoaderViewApi.propTypes = {
  size: PropTypes.oneOf(['small', 'large']),
  animating: PropTypes.bool,
};

CenterLoaderViewApi.defaultProps = { size: 'large', animating: true };

export default CenterLoaderViewApi;
