/* eslint-disable react-hooks/rules-of-hooks */
import { FlatList, RefreshControl } from "react-native";
import React, { useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

import {
  CenterLoaderViewApi,
  BottomLoaderViewApi,
  BottomErrorViewApi,
  ErrorViewApi,
  EmptyViewApi,
} from "../ApiViews";
import { AppStyles, Colors } from "@theme";
import { LIMIT } from "@WebService";
import { DataHandler, Util } from "@utils";

import { getRequestFlag, resetRequestFlags } from "@ducks/requestFlags";

// pagination type
export const PAGINATION_TYPE = { PAGE: "page", OFFSET: "offset" };

const FlatListApi = ({
  requestAction,
  actionType,
  renderItem,
  selectorData,
  keyExtractor,
  selectorItem,
  identifier,
  resetDataOnFirstLoad,
  resetActionOnUnMount,
  payload,
  filters,
  apiUrl,
  disableRequest,
  sendRequestOnMount,
  disableLoadMore,
  paginationType,
  limitKey,
  disableLimit,
  limit,
  pageNumberKey,
  pageKeyPayload,
  totalRecordsKey,
  idKeyForPagination,
  offsetKeyForPagination,
  emptyView,
  centerLoaderView,
  bottomLoaderView,
  errorView,
  bottomErrorView,
  emptyViewText,
  emptyViewDescription,
  emptyViewImage,
  forwardedRef,
  ListFooterComponent,
  showsVerticalScrollIndicator,
  showsHorizontalScrollIndicator,
  style,
  contentContainerStyle,
  keyboardShouldPersistTaps,
  rereshControlColor,
  ...rest
}) => {
  /************************************************
   *  redux data
   ************************************************/
  const requestFlagIdentifier = identifier
    ? `${actionType}_${identifier}`
    : actionType;
  const requestFlags = useSelector(getRequestFlag(requestFlagIdentifier));
  const data = identifier
    ? useSelector(selectorData(identifier))
    : useSelector(selectorData);
  const itemsData = selectorItem ? useSelector(selectorItem) : undefined;

  /************************************************
   *  filter ref
   ************************************************/
  const filtersRef = useRef();

  /************************************************
   *  request flags
   ************************************************/
  const {
    loading,
    failure,
    reset,
    isPullToRefresh,
    errorMessage,
    page,
    lastRecordsLength,
  } = requestFlags;

  /************************************************
   *  use effects
   ************************************************/
  // send request on mount
  useEffect(() => {
    if (sendRequestOnMount && !disableRequest) {
      sendRequestOnFirstLoad();
    }

    return () => {
      // reset request flags
      DataHandler.dispatchAction(
        resetRequestFlags({ reducerKey: requestFlagIdentifier })
      );

      // if we have reset action
      if (resetActionOnUnMount) {
        DataHandler.dispatchAction(
          resetActionOnUnMount({ reducerKey: requestFlagIdentifier })
        );
      }
    };
  }, []);

  // show Alert message on failure request
  useEffect(() => {
    if (
      Util.isNotEmpty(requestFlags) &&
      !loading &&
      failure &&
      data.length > 0 &&
      errorMessage
    ) {
      Util.showMessage(errorMessage);
    }
  }, [requestFlags]);

  // send request when update filters
  useEffect(() => {
    if (filtersRef.current && Util.compareDeep(filtersRef.current, filters)) {
      sendRequest(true, false, true);
    }
    filtersRef.current = filters;
  }, [filters]);

  /************************************************
   *  request functions
   ************************************************/
  // send request function
  const sendRequest = (
    resetRequest = false,
    isPullToRefreshRequest = false,
    resetReducerData = false
  ) => {
    // check if request is disabled
    if (disableRequest) {
      return;
    }

    // set the payload api
    let requestPayload = { ...payload, ...filters };
    // set  limit
    if (!disableLimit) {
      requestPayload[limitKey] = limit;
    }
    // set pagination
    if (paginationType === PAGINATION_TYPE.PAGE) {
      // set pagination for page type
      requestPayload[pageKeyPayload] = resetRequest
        ? 1
        : (page?.[pageNumberKey] ?? 1) + 1;
    } else if (
      !resetRequest &&
      paginationType === PAGINATION_TYPE.OFFSET &&
      data.length > 0
    ) {
      // set offset for pagination
      const lastRecordIndex = data.length - 1;
      requestPayload[offsetKeyForPagination] =
        data?.[lastRecordIndex]?.[idKeyForPagination] ?? "";
    }

    // set payload for request as we can only pass 1 payload in current structure
    const payloadAction = {
      payloadApi: requestPayload,
      reset: resetRequest,
      isPullToRefresh: isPullToRefreshRequest,
      resetReducerData,
    };
    if (identifier) {
      payloadAction.identifier = identifier;
    }
    if (apiUrl) {
      payloadAction.apiUrl = apiUrl;
    }

    // dispatch the action
    DataHandler.dispatchAction(requestAction(payloadAction));
  };

  // send request on first load
  const sendRequestOnFirstLoad = () => {
    sendRequest(true, false, resetDataOnFirstLoad);
  };

  // send request on refresh
  const onRefresh = () => {
    sendRequest(true, true);
  };

  // send request on load more
  const sendRequestLoadMore = () => {
    sendRequest(false, false);
  };

  const checkListHasRecords = () => {
    if (paginationType === PAGINATION_TYPE.OFFSET) {
      // for offset
      return lastRecordsLength >= limit;
    }

    // get length of data and total records
    const dataLength = data?.length ?? 0;
    const totalRecords = page?.[totalRecordsKey] ?? 0;
    return dataLength < totalRecords;
  };

  // send request on end reach
  const onEndReached = () => {
    // check if need to send request
    const sendRequestOnEnd =
      Util.isNotEmpty(requestFlags) &&
      !loading &&
      !disableLoadMore &&
      !failure &&
      checkListHasRecords();

    if (sendRequestOnEnd) {
      sendRequest(false, false);
    }
  };

  /************************************************
   *  flatlist views
   ************************************************/
  // render footer component
  const renderListFooterComponent = () => {
    // check need to show bottom loader
    const showBottomLoader =
      Util.isNotEmpty(requestFlags) &&
      loading &&
      !isPullToRefresh &&
      !reset &&
      !failure &&
      checkListHasRecords();

    // check need to show bottom error
    const showBottomError =
      Util.isNotEmpty(requestFlags) &&
      !loading &&
      !isPullToRefresh &&
      !reset &&
      failure &&
      checkListHasRecords();

    // render footer of list
    return (
      <>
        {ListFooterComponent ? ListFooterComponent() : null}
        {showBottomLoader
          ? bottomLoaderView?.() ?? <BottomLoaderViewApi />
          : null}
        {showBottomError
          ? bottomErrorView?.(errorMessage, sendRequestLoadMore) ?? (
              <BottomErrorViewApi
                errorMessage={errorMessage}
                onPressRetry={sendRequestLoadMore}
              />
            )
          : null}
      </>
    );
  };

  // render empty view
  const renderEmptyView = () => {
    // no request is sent yet
    if (Util.isEmpty(requestFlags) && data.length === 0 && !disableRequest) {
      return null;
    }

    return (
      emptyView?.() ?? (
        <EmptyViewApi
          text={emptyViewText}
          description={emptyViewDescription}
          image={emptyViewImage}
        />
      )
    );
  };

  /************************************************
   *  center loader and error view
   ************************************************/
  const showCenterLoading = loading && !isPullToRefresh && data.length === 0;
  const showCenterError = failure && data.length === 0;

  // render loader
  if (showCenterLoading) {
    return centerLoaderView?.() ?? <CenterLoaderViewApi />;
  }

  // render error
  if (showCenterError) {
    return (
      errorView?.(errorMessage, sendRequestOnFirstLoad) ?? (
        <ErrorViewApi
          errorMessage={errorMessage}
          onPressRetry={sendRequestOnFirstLoad}
        />
      )
    );
  }

  /************************************************
   *  main render
   ************************************************/
  return (
    <FlatList
      data={data}
      keyExtractor={keyExtractor}
      onEndReached={onEndReached}
      ListEmptyComponent={renderEmptyView}
      onEndReachedThreshold={0.1}
      ref={forwardedRef}
      keyboardShouldPersistTaps={keyboardShouldPersistTaps}
      showsVerticalScrollIndicator={showsVerticalScrollIndicator}
      showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
      ListFooterComponent={renderListFooterComponent}
      style={[AppStyles.container, style]}
      contentContainerStyle={
        !data.length
          ? AppStyles.flex1
          : [AppStyles.flatlistContentContainer, contentContainerStyle]
      }
      refreshControl={
        <RefreshControl
          refreshing={isPullToRefresh || false}
          onRefresh={onRefresh}
          tintColor={rereshControlColor}
          colors={[rereshControlColor]}
        />
      }
      renderItem={
        itemsData
          ? ({ item }) => renderItem({ item: itemsData?.[item] ?? {} })
          : renderItem
      }
      {...rest}
    />
  );
};

FlatListApi.propTypes = {
  // required props
  requestAction: PropTypes.func.isRequired,
  actionType: PropTypes.string.isRequired,
  renderItem: PropTypes.func.isRequired,
  selectorData: PropTypes.func.isRequired,
  keyExtractor: PropTypes.func.isRequired,

  // optional props redux
  selectorItem: PropTypes.func,
  identifier: PropTypes.string,
  resetDataOnFirstLoad: PropTypes.bool,

  // optional actions
  resetActionOnUnMount: PropTypes.func,

  // optional props request
  payload: PropTypes.object,
  filters: PropTypes.object,
  apiUrl: PropTypes.object,
  disableRequest: PropTypes.bool,
  sendRequestOnMount: PropTypes.bool,
  disableLoadMore: PropTypes.bool,

  // pagination
  paginationType: PropTypes.string,
  limitKey: PropTypes.string,
  disableLimit: PropTypes.bool,
  limit: PropTypes.number,
  // for type PAGE
  pageNumberKey: PropTypes.string,
  pageKeyPayload: PropTypes.string,
  totalRecordsKey: PropTypes.string,
  // for type OFFSET
  idKeyForPagination: PropTypes.string,
  offsetKeyForPagination: PropTypes.string,

  // optional props api views
  emptyView: PropTypes.func,
  centerLoaderView: PropTypes.func,
  bottomLoaderView: PropTypes.func,
  errorView: PropTypes.func,
  bottomErrorView: PropTypes.func,

  // optional props empty view
  emptyViewText: PropTypes.string,
  emptyViewDescription: PropTypes.string,
  emptyViewImage: PropTypes.number,

  // optional props FlatList
  forwardedRef: PropTypes.any,
  ListFooterComponent: PropTypes.func,
  showsVerticalScrollIndicator: PropTypes.bool,
  showsHorizontalScrollIndicator: PropTypes.bool,
  contentContainerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  keyboardShouldPersistTaps: PropTypes.string,
  rereshControlColor: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

FlatListApi.defaultProps = {
  // optional props redux
  selectorItem: undefined,
  identifier: undefined,
  resetDataOnFirstLoad: false,

  // optional actions
  resetActionOnUnMount: undefined,

  // optional props request
  payload: {},
  filters: {},
  apiUrl: undefined,
  disableRequest: false,
  sendRequestOnMount: true,
  disableLoadMore: false,

  // pagination
  paginationType: PAGINATION_TYPE.PAGE,
  limitKey: "perPage",
  disableLimit: false,
  limit: LIMIT,
  // for type PAGE
  pageNumberKey: "page",
  pageKeyPayload: "page",
  totalRecordsKey: "total",
  // for type OFFSET
  idKeyForPagination: "_id",
  offsetKeyForPagination: "offset",

  // optional props api views
  emptyView: undefined,
  centerLoaderView: undefined,
  bottomLoaderView: undefined,
  errorView: undefined,
  bottomErrorView: undefined,

  // optional props empty view
  emptyViewText: "",
  emptyViewDescription: "",
  emptyViewImage: undefined,

  //  optional props FlatList
  forwardedRef: undefined,
  ListFooterComponent: undefined,
  showsVerticalScrollIndicator: false,
  showsHorizontalScrollIndicator: false,
  style: {},
  contentContainerStyle: {},
  keyboardShouldPersistTaps: "handled",
  rereshControlColor: Colors.primary,
};

export default FlatListApi;
