import { ActivityIndicator, View } from 'react-native';
import React from 'react';

import { Colors } from '@theme';
import styles from './styles';

const BottomLoaderViewApi = ({}) => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="small" color={Colors.primary} />
    </View>
  );
};

BottomLoaderViewApi.propTypes = {};

export default BottomLoaderViewApi;
