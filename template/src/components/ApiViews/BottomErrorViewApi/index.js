import { View, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import { ButtonView } from '@components';
import { Images } from '@theme';
import { strings } from '@il8n';

import styles from './styles';

const BottomErrorViewApi = ({ errorMessage, onPressRetry }) => {
  return (
    <View style={styles.container}>
      <Image source={Images.loadMoreError} />
      <Text style={styles.message}>{errorMessage}</Text>
      <ButtonView onPress={onPressRetry} style={styles.retryButton}>
        <Text>{strings('api_error_messages.retry').toLocaleUpperCase()}</Text>
      </ButtonView>
    </View>
  );
};

BottomErrorViewApi.propTypes = {
  errorMessage: PropTypes.string.isRequired,
  onPressRetry: PropTypes.func.isRequired,
};

export default BottomErrorViewApi;
