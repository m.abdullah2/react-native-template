import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { useRef } from 'react';
import * as yup from 'yup';

import { strings } from '@il8n';

const ValidationType = {
  required: 'required',
  minLength: 'min_length',
  character: 'character',
  confirmPassword: 'confirm_password',
  equalLength: 'equal_length',
  email: 'email',
  url: 'url',
};

const Regex = {
  lowerCase: /^(?=.*[a-z])/,
  upperCase: /^(?=.*[A-Z])/,
  numeric: /^(?=.*[0-9])/,
  special: /^(?=.*[!@#$%^&*])/,
  url: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm,
};

const displayMsg = (label, type) => {
  const _type = type ?? ValidationType.required;
  return strings(`validation.${_type}`, { key: 'label', value: label });
};

export const Validation = {
  required: (title, type = ValidationType.required, valueType = 'string') => {
    if (valueType === 'object') {
      return yup[valueType]().nullable(true).required(displayMsg(title, type));
    }
    return yup[valueType]()
      .nullable(true)
      .trim?.()
      .required(displayMsg(title, type));
  },

  notRequired: () => yup.string().notRequired(),

  email: title =>
    yup
      .string()
      .required(displayMsg(title))
      .email(displayMsg(title, ValidationType.email)),

  phone: title => yup.string().required(displayMsg(title)),

  requiredTitle: (
    title,
    type = ValidationType.required,
    valueType = 'string',
  ) => {
    if (valueType === 'object') {
      return yup[valueType]().nullable(true).required('AA');
    } else if (valueType === 'array') {
      return yup.array().min(1, 'bA');
    }

    return yup[valueType]().nullable(true).trim?.().required('Enter Title');
  },
  requiredDesc: (
    title,
    type = ValidationType.required,
    valueType = 'string',
  ) => {
    if (valueType === 'object') {
      return yup[valueType]().nullable(true).required('AA');
    } else if (valueType === 'array') {
      return yup.array().min(1, 'bA');
    }

    return yup[valueType]()
      .nullable(true)
      .trim?.()
      .required('Enter Description');
  },
  password: title =>
    yup
      .string()
      .required(displayMsg(title))
      .matches(/^(?=.{8,})/, displayMsg('8', ValidationType.minLength))
      .matches(
        Regex.lowerCase,
        displayMsg('lowerCase', ValidationType.character),
      )
      .matches(
        Regex.upperCase,
        displayMsg('upperCase', ValidationType.character),
      )
      .matches(Regex.numeric, displayMsg('numeric', ValidationType.character))
      .matches(Regex.special, displayMsg('special', ValidationType.character)),

  passwordMatch: (matchFieldName, label) =>
    yup
      .string()
      .required(displayMsg(label))
      .oneOf(
        [yup.ref(matchFieldName), null],
        displayMsg('', ValidationType.confirmPassword),
      ),

  length: (title, _length) =>
    yup
      .string()
      .required(displayMsg(title))
      .test('len', displayMsg(_length, ValidationType.equalLength), val => {
        const valueLength = val?.length ?? 0;
        return valueLength === _length;
      }),

  webUrl: (title, req) =>
    yup
      .string()
      .required(displayMsg(title))
      .matches(Regex.url, displayMsg(title, ValidationType.url)),
};

export const useHookField = (formObj, name) => {
  const { control, formState } = formObj;
  const { errors } = formState;
  const inputRef = useRef(null);

  const error = errors?.[name]?.message ?? undefined;

  return {
    forwardRef: inputRef,
    control,
    name,
    error,
  };
};

export const useHookForm = (
  inputs,
  defaultValues = {},
  resolver = undefined,
) => {
  const formObj = useForm({
    resolver: yupResolver(resolver),
    defaultValues: defaultValues,
  });
  const hookInputs = [formObj];
  for (let i = 0; i < inputs.length; i++) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    hookInputs.push(useHookField(formObj, inputs[i]));
  }
  return hookInputs;
};

export const ValidationSchema = {
  login: yup.object().shape({
    email: Validation.email('Email'),
    password: Validation.password('Password'),
  }),
  addPost: yup.object().shape({
    title: Validation.requiredTitle('Title'),
    body: Validation.requiredDesc('Description'),
  }),
};

export default { ValidationSchema, useHookField, useHookForm };

// import { yupResolver } from '@hookform/resolvers/yup';
// import { useForm } from 'react-hook-form';
// import { useRef } from 'react';
// import * as yup from 'yup';

// import { strings } from '@il8n';

// const ValidationType = {
//   required: 'required',
//   character: 'character',
//   email: 'email',
//   alphabetic: 'alphabetic',
//   alphanumeric: 'alphanumeric',
//   space: 'space',
// };

// const Regex = {
//   alphabets: /^[a-zA-Z ]+$/,
//   alphanumeric: /^[a-zA-Z0-9 ]+$/,
//   space: /^\S*$/,
//   numeric: /^(?=.*[0-9])/,
//   special: /^(?=.*[!@#$%^&*])/,
// };

// const displayMsg = (label, type) => {
//   const _type = type ?? ValidationType.required;
//   return strings(`validation.${_type}`, { key: 'label', value: label });
// };

// export const Validation = {
//   requiredName: (
//     title,
//     type = ValidationType.required,
//     valueType = 'string',
//   ) => {
//     if (valueType === 'object') {
//       return yup[valueType]().nullable(true).required('AA');
//     } else if (valueType === 'array') {
//       return yup.array().min(1, 'bA');
//     }

//     return yup[valueType]().nullable(true).trim?.().required('Enter Name');
//   },
//   requiredEmail: (
//     title,
//     type = ValidationType.required,
//     valueType = 'string',
//   ) => {
//     if (valueType === 'object') {
//       return yup[valueType]().nullable(true).required('AA');
//     } else if (valueType === 'array') {
//       return yup.array().min(1, 'bA');
//     }

//     return yup[valueType]().nullable(true).trim?.().required('Enter Email');
//   },
//   requiredDob: (
//     title,
//     type = ValidationType.required,
//     valueType = 'string',
//   ) => {
//     if (valueType === 'object') {
//       return yup[valueType]().nullable(true).required('AA');
//     } else if (valueType === 'array') {
//       return yup.array().min(1, 'bA');
//     }

//     return yup[valueType]()
//       .nullable(true)
//       .trim?.()
//       .required('Enter Date of Birth');
//   },
//   requiredGender: (
//     title,
//     type = ValidationType.required,
//     valueType = 'string',
//   ) => {
//     if (valueType === 'object') {
//       return yup[valueType]().nullable(true).required('AA');
//     } else if (valueType === 'array') {
//       return yup.array().min(1, 'bA');
//     }

//     return yup[valueType]().nullable(true).trim?.().required('Select Gender');
//   },
//   requiredTitle: (
//     title,
//     type = ValidationType.required,
//     valueType = 'string',
//   ) => {
//     if (valueType === 'object') {
//       return yup[valueType]().nullable(true).required('AA');
//     } else if (valueType === 'array') {
//       return yup.array().min(1, 'bA');
//     }

//     return yup[valueType]().nullable(true).trim?.().required('Enter Title');
//   },
//   requiredDesc: (
//     title,
//     type = ValidationType.required,
//     valueType = 'string',
//   ) => {
//     if (valueType === 'object') {
//       return yup[valueType]().nullable(true).required('AA');
//     } else if (valueType === 'array') {
//       return yup.array().min(1, 'bA');
//     }

//     return yup[valueType]()
//       .nullable(true)
//       .trim?.()
//       .required('Enter Description');
//   },
//   notRequired: () => yup.string().notRequired(),

//   email: title =>
//     yup.string().required('Enter Email').email('Enter Valid Email'),

//   checkFieldEmpty: (checkFieldName, label) =>
//     yup
//       .string()
//       .nullable()
//       .test('checkField', displayMsg(label), function (val) {
//         const checkFieldValue = this.parent?.[checkFieldName] ?? '';
//         console.log('condition', checkFieldValue !== '' || val !== '');
//         //return true;
//         return checkFieldValue !== '' || val !== '';
//       }),
// };

// export const useHookField = (formObj, name) => {
//   const { control, formState } = formObj;
//   const { errors } = formState;
//   const inputRef = useRef(null);

//   const error = errors?.[name]?.message ?? undefined;

//   return {
//     forwardRef: inputRef,
//     control,
//     name,
//     error,
//   };
// };

// export const useHookForm = (
//   inputs,
//   defaultValues = {},
//   resolver = undefined,
// ) => {
//   const formObj = useForm({
//     resolver: yupResolver(resolver),
//     defaultValues: defaultValues,
//   });
//   const hookInputs = [formObj];
//   for (let i = 0; i < inputs.length; i++) {
//     // eslint-disable-next-line react-hooks/rules-of-hooks
//     hookInputs.push(useHookField(formObj, inputs[i]));
//   }
//   return hookInputs;
// };

// export const ValidationSchema = {
//   personalInfoForm: yup.object().shape({
//     name: Validation.requiredName(),
//     email: Validation.email('Email'),
//     dateOfBirth: Validation.requiredDob('Date of Birth'),
//   }),
//   addPost: yup.object().shape({
//     title: Validation.requiredTitle('Title'),
//     body: Validation.requiredDesc('Description'),
//   }),
// };

// export default { ValidationSchema, useHookField, useHookForm };
