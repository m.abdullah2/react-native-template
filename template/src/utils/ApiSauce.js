import { eventChannel, END } from 'redux-saga';
import { create } from 'apisauce';

import {
  API_LOG,
  BASE_URL,
  API_TIMEOUT,
  REQUEST_TYPE,
  X_API_TOKEN,
  API_UPLOAD_FILE,
} from '@WebService';

import { Util, DataHandler } from '@utils';
import { strings } from '@il8n';

const api = create({
  baseURL: BASE_URL,
  timeout: API_TIMEOUT,
});

async function callRequestFileUpload(uri) {
  const payload = new FormData();
  const photo = { uri, type: 'image/jpeg', name: 'image.jpg' };
  payload.append('file', photo);
  const url = API_UPLOAD_FILE;
  const headers = {};

  const { route, access_token_required } = url;
  // set X-API-TOKEN
  if (access_token_required) {
    /* const token = authSelectors.getUserToken(DataHandler.getStore());
    headers[X_API_TOKEN] = token; */
  }

  // init header object
  const headerObject = { headers };

  // init responseoc
  let response = await api.post(route, payload, headerObject);

  // log web service response
  if (__DEV__ && API_LOG) {
    console.log('url', url);
    console.log('response', response);
    console.log('payload', payload);
    console.log('headers', headers);
  }

  return handleResponse(response, headers);
}

async function callRequest(url, payload, headers = {}, parameter = '') {
  // get attributes from url

  const { type, access_token_required } = url;
  // set X-API-TOKEN
  if (access_token_required) {
    //headers[X_API_TOKEN] = 'def36000-f034-48f5-bf38-cd3d7cef2a5e';
    /*  const storeRef = DataHandler.getStore().getState();
    headers[X_API_TOKEN] = token; */
  }

  const route =
    parameter && parameter !== '' ? url.route + '/' + parameter : url.route;

  headers['Content-Type'] = 'application/json';

  // init header object
  const headerObject = { headers };

  // init responseoc
  let response;

  // on type send request
  switch (type) {
    case REQUEST_TYPE.GET:
      response = await api.get(route, payload, headerObject);
      break;
    case REQUEST_TYPE.POST:
      response = await api.post(route, payload, headerObject);
      break;
    case REQUEST_TYPE.DELETE:
      response = await api.delete(
        route,
        {},
        { data: payload, ...headerObject },
      );
      //response = await api.delete(route, payload, headerObject);
      break;
    case REQUEST_TYPE.PUT:
      response = await api.put(route, payload, headerObject);
      break;
    case REQUEST_TYPE.PATCH:
      response = await api.patch(route, payload, headerObject);
      break;
    default:
      response = await api.get(route, payload, headerObject);
  }

  // log web service response
  if (__DEV__ && API_LOG) {
    console.log('url', url);
    console.log('response', response);
    console.log('payload', payload);
    console.log('headers', headers);
    console.log('route', route);
  }

  return handleResponse(response, headers);
}

function handleResponse(response, headers) {
  return new Promise((resolve, reject) => {
    // network error  internet not working
    const isNetWorkError = response.problem === 'NETWORK_ERROR';
    // network error  internet not working
    const isClientError = response.problem === 'CLIENT_ERROR';
    // kick user from server
    const status = response?.status ?? 500;
    const isKickUser = status === 403;
    // if response is valid
    const isResponseValid =
      response.ok && Util.isNotEmpty(response.data) ? true : false;

    if (isResponseValid) {
      resolve(response.data);
    } else if (isNetWorkError) {
      if (DataHandler.getIsInternetConnected()) {
        reject({
          message: strings('api_error_messages.something_went_wrong'),
          statusCode: status,
        });
      } else {
        reject({
          message: strings('api_error_messages.network_not_available'),
          statusCode: status,
        });
      }
    } else if (isKickUser) {
      Util.showMessage(strings('api_error_messages.kick_user', 10000));
      //NavigationService.reset('Main');
    } else if (isClientError) {
      reject({
        message:
          response.data &&
          response.data.msg &&
          typeof response.data.msg === 'string'
            ? response.data.msg
            : strings('api_error_messages.something_went_wrong'),
        statusCode: status,
      });
    } else {
      reject({
        message: strings('api_error_messages.something_went_wrong'),
        statusCode: status,
      });
    }
  });
}

function postWithProgress(url, data, headers = {}) {
  // get attributes from url

  const { route, access_token_required } = url;

  // set X-API-TOKEN
  if (access_token_required) {
    /* const token = authSelectors.getUserToken(DataHandler.getStore());
    headers[X_API_TOKEN] = token; */
  }

  return eventChannel(emitter => {
    api
      .post(route, data, {
        headers,
        onUploadProgress: e => {
          if (e.lengthComputable) {
            const progress = Math.round((e.loaded / e.total) * 100);
            emitter({ progress });
          }
        },
      })
      .then(
        response => {
          if (response.ok && response.data) {
            emitter({ success: response.data });
            emitter(END);
          } else if (response.problem === 'NETWORK_ERROR') {
            emitter({
              err: strings('api_error_messages.network_not_available'),
            });
            emitter(END);
          } else {
            emitter({
              err: strings('api_error_messages.something_went_wrong'),
            });
            emitter(END);
          }
        },
        err => {
          if (err.problem === 'NETWORK_ERROR') {
            emitter({
              err: strings('api_error_messages.network_not_available'),
            });
            emitter(END);
          } else {
            emitter({
              err: strings('api_error_messages.something_went_wrong'),
            });
            emitter(END);
          }
        },
      );

    return () => {};
  });
}

export { callRequestFileUpload, callRequest, postWithProgress };
