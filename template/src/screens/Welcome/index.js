import { View } from 'react-native';

import { NavigationService } from '@utils';
import { Text } from '@components';

import React from 'react';

const Welcome = () => {
  return (
    <View style={{ padding: 30, flex: 1, justifyContent: 'center' }}>
      <Text
        style={{ padding: 30 }}
        onPress={() => {
          NavigationService.navigate('Home');
        }}>
        Welcome To Cubix React Native Starter. Project is all setup and you are
        Good to go. Happy Coding Click to see updated FlatlistApi Example
      </Text>
    </View>
  );
};

export default Welcome;
