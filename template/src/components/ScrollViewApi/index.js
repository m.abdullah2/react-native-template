/* eslint-disable react-hooks/rules-of-hooks */
import { ScrollView, RefreshControl, View } from "react-native";
import React, { useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

import { CenterLoaderViewApi, ErrorViewApi, EmptyViewApi } from "../ApiViews";
import { DataHandler, Util } from "@utils";
import { Colors, AppStyles } from "@theme";

import { getRequestFlag, resetRequestFlags } from "@ducks/requestFlags";

const ScrollViewApi = ({
  requestAction,
  actionType,
  selectorData,
  contentView,
  identifier,
  resetDataOnFirstLoad,
  resetActionOnUnMount,
  payload,
  filters,
  apiUrl,
  sendRequestOnMount,
  emptyView,
  centerLoaderView,
  errorView,
  emptyViewText,
  emptyViewDescription,
  emptyViewImage,
  isViewOnly,
  forwardedRef,
  showsVerticalScrollIndicator,
  showsHorizontalScrollIndicator,
  style,
  contentContainerStyle,
  keyboardShouldPersistTaps,
  rereshControlColor,
  disableRequest,
  isRefreshControlEnable,
}) => {
  /************************************************
   *  redux data
   ************************************************/
  const requestFlagIdentifier = identifier
    ? `${actionType}_${identifier}`
    : actionType;
  const requestFlags = useSelector(getRequestFlag(requestFlagIdentifier));
  const data = identifier
    ? useSelector(selectorData(identifier))
    : useSelector(selectorData);

  /************************************************
   *  filter ref
   ************************************************/
  const filtersRef = useRef();

  /************************************************
   *  request flags
   ************************************************/
  const { loading, failure, isPullToRefresh, errorMessage } = requestFlags;

  /************************************************
   *  use effects
   ************************************************/
  // send request on mount
  useEffect(() => {
    if (sendRequestOnMount) {
      sendRequestOnFirstLoad();
    }

    return () => {
      // reset request flags
      DataHandler.dispatchAction(
        resetRequestFlags({ reducerKey: requestFlagIdentifier })
      );

      // if we have reset action
      if (resetActionOnUnMount) {
        DataHandler.dispatchAction(
          resetActionOnUnMount({ reducerKey: requestFlagIdentifier })
        );
      }
    };
  }, []);

  // show Alert message on failure request
  useEffect(() => {
    if (
      Util.isNotEmpty(requestFlags) &&
      !loading &&
      failure &&
      Util.isNotEmpty(data) &&
      errorMessage
    ) {
      Util.showMessage(errorMessage);
    }
  }, [requestFlags]);

  // send request when update filters
  useEffect(() => {
    if (filtersRef.current && Util.compareDeep(filtersRef.current, filters)) {
      sendRequest(true, false, true);
    }
    filtersRef.current = filters;
  }, [filters]);

  /************************************************
   *  request functions
   ************************************************/
  // send request function
  const sendRequest = (
    isPullToRefreshRequest = false,
    resetReducerData = false
  ) => {
    // check if request is disabled
    if (disableRequest) {
      return;
    }

    // set the payload api
    let requestPayload = { ...payload, ...filters };

    // set payload for request as we can only pass 1 payload in current structure
    const payloadAction = {
      payloadApi: requestPayload,
      isPullToRefresh: isPullToRefreshRequest,
      resetReducerData,
    };
    if (identifier) {
      payloadAction.identifier = identifier;
    }
    if (apiUrl) {
      payloadAction.apiUrl = apiUrl;
    }

    // dispatch the action
    DataHandler.dispatchAction(requestAction(payloadAction));
  };

  // send request on first load
  const sendRequestOnFirstLoad = () => {
    sendRequest(false, resetDataOnFirstLoad);
  };

  // send request on refresh
  const onRefresh = () => {
    sendRequest(true, false);
  };

  /************************************************
   *  center loader and error view and empty view
   ************************************************/
  const showCenterLoading = loading && !isPullToRefresh && data.length === 0;
  const showCenterError = failure && data.length === 0;
  const showEmptyView =
    Util.isNotEmpty(requestFlags) &&
    Util.isEmpty(data) &&
    !loading &&
    !failure &&
    !disableRequest;

  // render loader
  if (showCenterLoading) {
    return centerLoaderView?.() ?? <CenterLoaderViewApi />;
  }

  // render error
  if (showCenterError) {
    return (
      errorView?.(errorMessage, sendRequestOnFirstLoad) ?? (
        <ErrorViewApi
          errorMessage={errorMessage}
          onPressRetry={sendRequestOnFirstLoad}
        />
      )
    );
  }

  // render empty view
  if (showEmptyView) {
    return (
      emptyView?.() ?? (
        <EmptyViewApi
          text={emptyViewText}
          description={emptyViewDescription}
          image={emptyViewImage}
        />
      )
    );
  }

  /************************************************
   *  render main content
   ************************************************/
  if (Util.isNotEmpty(data)) {
    // check if is view only
    if (isViewOnly) {
      return (
        <View style={[AppStyles.container, style]}>{contentView(data)}</View>
      );
    }

    // refresh control
    const refreshControl = isRefreshControlEnable ? (
      <RefreshControl
        refreshing={isPullToRefresh}
        onRefresh={onRefresh}
        tintColor={rereshControlColor}
        colors={[rereshControlColor]}
      />
    ) : null;
    return (
      <ScrollView
        style={[AppStyles.container, style]}
        refreshControl={refreshControl}
        ref={forwardedRef}
        keyboardShouldPersistTaps={keyboardShouldPersistTaps}
        showsVerticalScrollIndicator={showsVerticalScrollIndicator}
        showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
        contentContainerStyle={contentContainerStyle}
      >
        {contentView(data)}
      </ScrollView>
    );
  }

  return null;
};

ScrollViewApi.propTypes = {
  // required props
  requestAction: PropTypes.func.isRequired,
  actionType: PropTypes.string.isRequired,
  selectorData: PropTypes.func.isRequired,
  contentView: PropTypes.func.isRequired,

  // optional props redux
  identifier: PropTypes.string,
  resetDataOnFirstLoad: PropTypes.bool,

  // optional actions
  resetActionOnUnMount: PropTypes.func,

  // optional props request
  payload: PropTypes.object,
  filters: PropTypes.object,
  apiUrl: PropTypes.object,
  disableRequest: PropTypes.bool,
  sendRequestOnMount: PropTypes.bool,

  // optional props api views
  emptyView: PropTypes.func,
  centerLoaderView: PropTypes.func,
  errorView: PropTypes.func,

  // optional props empty view
  emptyViewText: PropTypes.string,
  emptyViewDescription: PropTypes.string,
  emptyViewImage: PropTypes.number,

  //  optional props scroll view
  isViewOnly: PropTypes.bool,
  forwardedRef: PropTypes.any,
  showsVerticalScrollIndicator: PropTypes.bool,
  showsHorizontalScrollIndicator: PropTypes.bool,
  contentContainerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  keyboardShouldPersistTaps: PropTypes.string,
  rereshControlColor: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  isRefreshControlEnable: PropTypes.bool,
};

ScrollViewApi.defaultProps = {
  // optional props redux
  identifier: undefined,
  resetDataOnFirstLoad: false,

  // optional actions
  resetActionOnUnMount: undefined,

  // optional props request
  payload: {},
  filters: {},
  apiUrl: undefined,
  disableRequest: false,
  sendRequestOnMount: true,

  // optional props api views
  emptyView: undefined,
  centerLoaderView: undefined,
  errorView: undefined,

  // optional props empty view
  emptyViewText: "",
  emptyViewDescription: "",
  emptyViewImage: undefined,

  //  optional props scroll view
  isViewOnly: false,
  forwardedRef: undefined,
  showsVerticalScrollIndicator: false,
  showsHorizontalScrollIndicator: false,
  style: {},
  contentContainerStyle: {},
  keyboardShouldPersistTaps: "handled",
  rereshControlColor: Colors.primary,
  isRefreshControlEnable: true,
};

export default ScrollViewApi;
