import { Text } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import ImageLoad from '../ImageLoad';
import Image from '../Image';

const ImageViewHttp = props => {
  const { isShowActivity, cache, url, isLocal, source, headers, ...rest } =
    props;

  if (isLocal || typeof url === 'number') {
    return <Image {...rest} source={url} />;
  }

  return url || source ? (
    <ImageLoad
      isShowActivity={isShowActivity}
      source={source ?? { uri: url, cache, headers }}
      placeholderStyle={{
        width: props?.style?.width / 1.5 ?? 0,
        height: props?.style?.height / 1.5 ?? 0,
      }}
      {...rest}
    />
  ) : (
    <ImageLoad
      placeholderStyle={{
        width: props?.style?.width / 1.5 ?? 0,
        height: props?.style?.height / 1.5 ?? 0,
      }}
      {...rest}
      isShowActivity={false}
      source={{ uri: 'dummy' }}
    />
  );
};

ImageViewHttp.propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  isShowActivity: PropTypes.bool,
  url: PropTypes.any,
  isLocal: PropTypes.bool,
  cache: PropTypes.string,
  source: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  headers: PropTypes.object,
};

ImageViewHttp.defaultProps = {
  isShowActivity: false,
  url: '',
  style: {},
  isLocal: false,
  cache: 'default',
  headers: {},
};

export default React.memo(ImageViewHttp, (prevProps, nextProps) => {
  return prevProps.url === nextProps.url;
});
