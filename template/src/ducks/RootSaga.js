import { fork } from 'redux-saga/effects';
import addPost from './addPost/saga';

export default function* root() {
  yield fork(addPost);
}
