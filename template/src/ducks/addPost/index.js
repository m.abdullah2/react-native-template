import { makeRequesActions } from '@ActionTypes';
import { createReducer } from '@reduxjs/toolkit';

// action creators
export const addPost = makeRequesActions('ADD_POST');
export const getPostListing = makeRequesActions('GET_POST');

// inital state
const initialState = { data: [] };

// init reducer
export default createReducer(initialState, builder => {
  // simple list
  builder.addCase(addPost.success, (state, action) => {});
  builder.addCase(getPostListing.success, (state, action) => {
    state.data = action.payload.data;
  });
});

const defaultList = [];
// selectors
export const getPostList = state => state.addPost.data ?? defaultList;
