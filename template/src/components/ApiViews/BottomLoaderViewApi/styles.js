import { StyleSheet } from 'react-native';
import { Metrics } from '@theme';

export default StyleSheet.create({
  container: {
    paddingTop: Metrics.baseMargin,
    paddingBottom: Metrics.bottomPadding,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
