import FlashMessage from 'react-native-flash-message';
import React, { useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import { View } from 'react-native';

import { ConfigureApp, DataHandler, NetworkInfo } from '@utils';
import AppNavigator from '@navigator';
import { DatePickerModal, TopLoader } from '@models';
import configureStore from '@store';
import { AppStyles } from '@theme';
import SplashScreen from 'react-native-splash-screen';

// configure app
ConfigureApp();

const App = () => {
  // set store state
  const [storeState, setStore] = useState(null);

  // when store is configured
  const onStoreConfigure = store => {
    //init things
    DataHandler.setStore(store);
    NetworkInfo.addNetInfoListener();

    // set store state
    setStore(store);

    // hide splash
    // SplashScreen.hide();
  };

  useEffect(() => {
    // configure store
    configureStore(onStoreConfigure);

    // unscribe to all things on unmount
    return () => {
      NetworkInfo.removeNetInfoListener();
    };
  }, []);

  if (storeState === null) {
    return null;
  }

  return (
    <View style={AppStyles.flex1}>
      <Provider store={storeState}>
        <AppNavigator />
        <FlashMessage position="top" titleStyle={AppStyles.flashMessage} />
        <TopLoader ref={ref => DataHandler.setTopLoaderRef(ref)} />
        <DatePickerModal ref={ref => DataHandler.setDatePickerModalRef(ref)} />
      </Provider>
    </View>
  );
};
export default App;
