module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          '@assets': './src/assets',
          '@chatUtils': './src/chatUtils',
          '@common': './src/common',
          '@components': './src/components',
          '@config': './src/config',
          '@dataUtils': './src/dataUtils',
          '@ducks': './src/ducks',
          '@jsonData': './src/jsonData',
          '@models': './src/models',
          '@navigator': './src/navigator',
          '@screens': './src/screens',
          '@store': './src/store',
          '@theme': './src/theme',
          '@translations': './src/translations',
          '@utils': './src/utils',
          '@il8n': './src/utils/i18n',
          '@ActionTypes': './src/ducks/ActionTypes',
          '@WebService': './src/config/WebService',
          '@Constants': './src/config/Constants',
          '@ApiSauce': './src/utils/ApiSauce',
          '@ValidationUtil': './src/utils/ValidationUtil',
        },
      },
    ],
  ],
};
