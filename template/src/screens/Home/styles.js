import { StyleSheet } from "react-native";
import { Colors, Fonts, Metrics } from "@theme";

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  text: { padding: Metrics.baseMargin },
  rendeItem: {
    padding: 10,
    marginHorizontal: Metrics.ratio(20),
    marginVertical: Metrics.ratio(25),
  },
  renderItemTitle: {
    fontWeight: "200",
    marginVertical: Metrics.ratio(20),
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    height: Metrics.ratio(55),
    backgroundColor: Colors.primary,
    borderRadius: Metrics.ratio(5),
    marginHorizontal: Metrics.ratio(20),
    marginBottom: Metrics.bottomPadding,
  },

  title: {
    // fontFamily: Fonts.type.medium,
    fontWeight: "200",
    fontSize: Fonts.size.size_17,
    textAlign: "center",
    color: Colors.white,
  },
});
