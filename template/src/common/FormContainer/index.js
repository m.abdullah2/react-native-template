import { Keyboard, ScrollView, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import { AppStyles, Colors } from '@theme';

import styles from './styles';

const FormContainer = ({
  buttonText,
  buttonPress,
  children,
  containerStyle,
}) => {
  const onButtonPress = () => {
    Keyboard.dismiss();
    buttonPress();
  };
  return (
    <>
      <ScrollView
        style={[AppStyles.container3, containerStyle]}
        contentContainerStyle={styles.container}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled">
        {children}
      </ScrollView>
      {buttonPress && (
        <TouchableOpacity style={styles.button} onPress={onButtonPress}>
          <Text color={Colors.white} style={styles.title}>
            {buttonText}
          </Text>
        </TouchableOpacity>
      )}
    </>
  );
};

FormContainer.propTypes = {
  buttonText: PropTypes.string,
  buttonPress: PropTypes.func,
  children: PropTypes.any.isRequired,
};
FormContainer.defaultProps = {
  buttonText: '',
  buttonPress: undefined,
};

export default FormContainer;
