import { TextInput as Input, View, Text, Image } from 'react-native';
import { Controller } from 'react-hook-form';
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { ButtonView } from '@components';
import { Colors, AppStyles, Images } from '@theme';
import { strings } from '@il8n';

import styles from './styles';

const TextInput = props => {
  // destruct props
  const {
    control,
    name,
    forwardRef,
    title,
    renderRightBottom,
    nextFocusRef,
    error,
    customPlaceholder,
    renderLeft,
    renderRight,
    required,
    showCharCount,
    maxLength,
    onPress,
    onSubmit,
    multiline,
    multlineStyle,
    containerStyle,
    dropdownKey,
    formatValue,
    textAlign,
    setMultlineStyle,
    showTitle,
    customTitle,
    bottomSpaceLarge,
    topSpaceLarge,
    formatValueChange,
    disablePress,
    secureTextEntry,
    onChangeCustom,
    isRightArrow,
    editable,
    optional,
    dropdownImageKey,
    ...rest
  } = props;

  // set state focus
  const [isFocused, setFocus] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  // render input
  const renderInput = ({ onChange, onBlur: _onBlur, value }) => {
    // input events
    const onChangeText = textInputValue => {
      if (formatValueChange) {
        onChange(formatValueChange(textInputValue));
      } else {
        onChange(textInputValue);
      }
      if (onChangeCustom) {
        onChangeCustom(textInputValue);
      }
    };

    const onBlur = () => {
      _onBlur();
      setFocus(false);
    };
    const onFocus = () => {
      setFocus(true);
    };

    const onSubmitEditing = () => {
      if (nextFocusRef) {
        nextFocusRef.current.focus();
      }
    };

    // set placeholder text
    const placeholder = onPress
      ? `${strings('validation.select')} ${title}`
      : `${strings('validation.enter')} ${title}`;

    const opacity = disablePress || editable === false ? 0.55 : 1;

    // set input value for dropdown
    const inputValue = dropdownKey ? value?.[dropdownKey] ?? '' : value || '';

    // custom style
    const customStyleMulti =
      multiline && setMultlineStyle
        ? { ...multlineStyle, opacity }
        : { opacity };

    // render input

    return (
      <Input
        style={[
          styles.input,
          customStyleMulti,
          { textAlign },
          multiline && { textAlignVertical: 'top' },
        ]}
        placeholderTextColor={'black'}
        placeholder={customPlaceholder || placeholder}
        value={formatValue ? formatValue(inputValue) : `${inputValue}`}
        ref={forwardRef}
        returnKeyType={onSubmit ? 'done' : 'next'}
        onSubmitEditing={onSubmit || onSubmitEditing}
        editable={_.isUndefined(editable) ? (onPress ? false : true) : editable}
        pointerEvents={onPress ? 'none' : 'auto'}
        selection={onPress ? { start: 0, end: 0 } : undefined}
        autoCapitalize="none"
        autoCorrect={false}
        secureTextEntry={secureTextEntry && !showPassword}
        {...{
          maxLength,
          onChangeText,
          onBlur,
          onFocus,
          multiline,
        }}
        {...rest}
      />
    );
  };

  //render title
  const renderTitle = () => {
    if (showTitle) {
      return (
        <View style={styles.titleView}>
          <Text style={styles.title} color={Colors.primary}>{`${
            customTitle || title
          }${required ? '*' : ''}`}</Text>
          {optional ? (
            <Text style={styles.optionalText}>
              {strings('app.optional_with_brackets')}
            </Text>
          ) : null}
        </View>
      );
    }
    return null;
  };

  const renderLeftInput = controlllerProps => {
    if (dropdownImageKey) {
      return <Text>Hi</Text>;
    }
    if (renderLeft) {
      return renderLeft();
    }
    return null;
  };

  const renderRightInput = () => {
    if (renderRight) {
      return renderRight();
    } else if (secureTextEntry) {
      const eyeImage = showPassword
        ? Images.images.visibility
        : Images.images.visibilityOff;
      return (
        <ButtonView
          style={styles.eyeContainer}
          onPress={() => {
            setShowPassword(!showPassword);
          }}>
          <Image source={eyeImage} />
        </ButtonView>
      );
    } else if (onPress) {
      return <Text></Text>;
    }
    return null;
  };

  // render input container
  const renderInputContainer = controlllerProps => {
    // set border color
    let borderColor = Colors.paleGrey;
    if (error) {
      borderColor = Colors.errorInput;
    } else if (isFocused) {
      borderColor = Colors.primary;
    }

    // set view tag
    const TagView = onPress && disablePress === false ? ButtonView : View;
    const opacity = disablePress || editable === false ? 0.55 : 1;

    return (
      <TagView
        style={[
          AppStyles.inputBoxStyle,
          styles.inputContainer,
          { borderColor, opacity },
        ]}
        onPress={() => {
          onPress(controlllerProps.onChange, controlllerProps.value);
        }}>
        {renderLeftInput(controlllerProps)}
        {renderInput(controlllerProps)}
        {renderRightInput()}
      </TagView>
    );
  };

  // render right bottom view
  const renderRightBottomView = ({ value }) => {
    if (renderRightBottom) {
      return renderRightBottom();
    } else if (showCharCount) {
      return (
        <Text color={Colors.primary}>
          {`${strings('validation.max_characters')} ${
            value?.length ?? 0
          }/${maxLength}`}
        </Text>
      );
    }
    return null;
  };

  // render error
  const renderErrorAndRight = controlllerProps => {
    if (error || renderRightBottom || showCharCount) {
      return (
        <View style={styles.bottomContainer}>
          <Text style={styles.errorView} color={Colors.errorInput}>
            {error || ''}
          </Text>
          {renderRightBottomView(controlllerProps)}
        </View>
      );
    }
    return null;
  };

  // render input controller
  const renderController = controlllerProps => {
    return (
      <View style={[styles.container, containerStyle]}>
        {renderTitle()}
        {renderInputContainer(controlllerProps.field)}
        {renderErrorAndRight(controlllerProps.field)}
      </View>
    );
  };

  return <Controller control={control} name={name} render={renderController} />;
};

TextInput.propTypes = {
  containerStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ]),
  multlineStyle: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
    PropTypes.number,
  ]),
  required: PropTypes.bool,
  control: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  forwardRef: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  customPlaceholder: PropTypes.string,
  onChangeCustom: PropTypes.func,
  nextFocusRef: PropTypes.object,
  error: PropTypes.object,
  onPress: PropTypes.func,
  renderLeft: PropTypes.func,
  renderRight: PropTypes.func,
  showCharCount: PropTypes.bool,
  maxLength: PropTypes.number,

  onSubmit: PropTypes.func,
  multiline: PropTypes.bool,
  dropdownKey: PropTypes.string,
  dropdownImageKey: PropTypes.string,
  formatValue: PropTypes.func,
  formatValueChange: PropTypes.func,
  setMultlineStyle: PropTypes.bool,
  textAlign: PropTypes.string,
  showTitle: PropTypes.bool,
  renderRightBottom: PropTypes.func,
  customTitle: PropTypes.string,
  bottomSpaceLarge: PropTypes.bool,
  topSpaceLarge: PropTypes.bool,
  disablePress: PropTypes.bool,
  secureTextEntry: PropTypes.bool,
  isRightArrow: PropTypes.bool,
  editable: PropTypes.bool,
  optional: PropTypes.bool,
};
TextInput.defaultProps = {
  containerStyle: {},
  multlineStyle: styles.multline,
  setMultlineStyle: true,
  required: false,
  error: undefined,
  nextFocusRef: undefined,
  onChangeCustom: undefined,
  formatValueChange: undefined,
  onPress: undefined,
  customPlaceholder: '',
  renderLeft: undefined,
  renderRight: undefined,
  renderRightBottom: undefined,
  showCharCount: false,
  dropdownImageKey: '',
  maxLength: 10000,
  onSubmit: undefined,
  multiline: false,
  dropdownKey: '',
  formatValue: undefined,
  textAlign: 'left',
  showTitle: true,
  customTitle: '',
  bottomSpaceLarge: false,
  topSpaceLarge: false,
  disablePress: false,
  secureTextEntry: false,
  isRightArrow: false,
  optional: false,
};

export default TextInput;
