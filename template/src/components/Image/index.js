import { Image as ImageView } from 'react-native';
import PropTypes from 'prop-types';
import React from 'react';

import { AppStyles } from '@theme';
import styles from './styles';

const Image = props => {
  const { style, isTransform, ...rest } = props;
  const imageStyleTransform = isTransform ? styles.imageStyle : AppStyles.empty;
  return <ImageView style={[imageStyleTransform, style]} {...rest} />;
};

Image.propTypes = {
  isTransform: PropTypes.bool,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

Image.defaultProps = {
  style: {},
  isTransform: true,
};

export default Image;
