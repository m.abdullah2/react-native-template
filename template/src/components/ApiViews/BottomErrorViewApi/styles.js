import { StyleSheet } from 'react-native';
import { Metrics, Fonts, Colors } from '@theme';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: Metrics.baseMargin,
    paddingBottom: Metrics.bottomPadding,
    paddingHorizontal: Metrics.baseMargin,
    justifyContent: 'center',
    alignItems: 'center',
  },
  message: {
    flex: 1,
    marginHorizontal: Metrics.midMargin,
    textAlign: 'center',
    lineHeight: Metrics.largeMargin,
    fontSize: Fonts.size.size_15,
    fontWeight: '400',
    // fontFamily: Fonts.type.medium,
  },
  retryButton: {
    paddingVertical: Metrics.smallMargin,
    paddingHorizontal: Metrics.baseMargin,
    borderRadius: Metrics.ratio(4),
    borderColor: Colors.black,
    borderWidth: Metrics.ratio(1),
  },
});
